FROM python:3.6

WORKDIR /tmp/src

COPY . .

RUN pip install .

RUN rm -rf /tmp/src
