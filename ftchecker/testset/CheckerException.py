class CheckerException(Exception):
    def __init__(self, error_message, cmd_out=[]):
        if len(cmd_out) == 0:
            super().__init__(f"{error_message}")
        else:
            cmd_out = '\n'.join(cmd_out)
            super().__init__(f"{error_message}\n--- ACTUAL RESPONSE:\n{cmd_out}\n---")
