from ftchecker.testset.Test import Test
from ftchecker.testset.WrongAnswerException import WrongAnswerException
import traceback


class AcceptanceCriteria:

    def __init__(self, _id, _title, _function_to_call, _dependency, _args):
        self.id = _id
        self.title = _title
        self.function_to_call = _function_to_call
        self.dependency = _dependency
        self.args = _args
        self.executed = False
        self.result = None
        self.return_message = None
        self.return_expected = None

    def restart(self):
        self.executed = False
        self.result = None
        self.return_message = None

    def run(self, context):
        self.executed = True
        try:
            print("Executing {}".format(self.title))
            self.function_to_call(context, self.args)
            self.result = True
        except WrongAnswerException:
            print("FAILED")
            self.result = False
            return
        except Exception as e:
            print("Error while running test \"{}\": ".format(self.title))
            print(e)
            if "DEBUG" in context and context["DEBUG"]:
                traceback.print_exc()
            return

        print("PASSED")

    def is_ran_successful(self):
        return self.result

    def is_executed(self):
        return self.executed

    def get_report(self):
        return [self.executed, self.result]
