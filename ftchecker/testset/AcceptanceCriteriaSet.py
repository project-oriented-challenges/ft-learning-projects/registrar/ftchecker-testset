from ftchecker.testset.AcceptanceCriteria import AcceptanceCriteria


class AcceptanceCriteriaSet:
    def __init__(self):
        self.acceptance_criteria_set = []

    def add_acceptance_criteria(self, acceptance_id, title, callback, dependency=None, args=None):
        acceptance = AcceptanceCriteria(acceptance_id, title, callback, dependency, args)
        self.acceptance_criteria_set.append(acceptance)
