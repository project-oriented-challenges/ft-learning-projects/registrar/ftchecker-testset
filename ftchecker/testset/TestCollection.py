from ftchecker.testset.Test import Test
from collections import defaultdict


class TestCollection:

    def __init__(self, pretest=None):
        self.pretest = pretest
        self.context = {}  # need to store results of function for executing other
        self.test_order = []  # stored tests id in order of execution
        self.test_collection = {}  # stores tests: key - id, value - function
        self.context['pretest_action'] = False
        self.acceptance_order = defaultdict(lambda: [])  # key - test_id, value - list of acceptance criteria id
        self.acceptance_collection = defaultdict(
            lambda: {})  # key - test_id, value - dict where key = acceptance criteria id, value - acceptance object

    def add_test(self, _id, _title, _func=None, acceptance_criteria_set=None, dependency=None, args=None):
        if dependency == 'all previous':
            dependency = self.test_order.copy()
        test = Test(_id, _title, _func, dependency, args)
        if acceptance_criteria_set is not None:
            for acceptance_obj in acceptance_criteria_set.acceptance_criteria_set:
                self.acceptance_order[_id].append(acceptance_obj.id)
                self.acceptance_collection[_id][acceptance_obj.id] = acceptance_obj
        self.test_collection[_id] = test
        self.test_order.append(_id)

    def add_item_to_context(self, name, value):
        self.context[name] = value

    def run_acceptance(self, test_id, acceptance_id):
        acceptance = self.acceptance_collection[test_id][acceptance_id]
        if acceptance.dependency is not None:
            if type(acceptance.dependency) == int:
                dep_list = [acceptance.dependency]
            elif type(acceptance.dependency) == list:
                dep_list = acceptance.dependency
            else:
                print('Incorrect dependency')
                return
            for ac_id in dep_list:
                acceptance_dependent_to = self.acceptance_collection[test_id][ac_id]
                if not acceptance_dependent_to.is_executed():
                    print("To run {}, need to run {}".format(acceptance.title,
                                                             acceptance_dependent_to.title))
                    self.run_acceptance(test_id, acceptance_dependent_to.id)
                if not acceptance_dependent_to.is_ran_successful():
                    print("Cant run \"{}\", because \"{}\" failed".format(acceptance.title,
                                                                          acceptance_dependent_to.title))
                    return

        if not self.context['pretest_action'] and self.pretest is not None:
            self.pretest(self.context)
            self.context['pretest_action'] = True

        acceptance.run(self.context)

    def run_test(self, init_test_id):
        test = self.test_collection[init_test_id]
        if test.dependency is not None:
            if type(test.dependency) == int:
                dep_list = [test.dependency]
            elif type(test.dependency) == list:
                dep_list = test.dependency
            else:
                print('Incorrect dependency')
                return
            for test_id in dep_list:
                test_depended_to = self.test_collection[test_id]
                if not test_depended_to.is_executed():
                    print(f"To run '{test.title}', need to run '{test_depended_to.title}'")
                    self.run_test(test_depended_to.id)
                if not test_depended_to.is_ran_successful():
                    if test_depended_to.is_executed():
                        print(f"Can't run '{test.title}', because '{test_depended_to.title}' failed")
                    return

        if not self.context['pretest_action'] and self.pretest is not None:
            self.pretest(self.context)
            self.context['pretest_action'] = True

        test.run(self.context)

        if test.is_ran_successful():
            acceptance_criteria_list = self.acceptance_order[init_test_id]
            for acceptance_id in acceptance_criteria_list:
                self.run_acceptance(init_test_id, acceptance_id)
            for acceptance_id in acceptance_criteria_list:
                if not self.acceptance_collection[init_test_id][acceptance_id].is_ran_successful():
                    test.result = False
                    break

    def run_all_test(self):
        for test_id in self.test_order:
            self.run_test(test_id)

    def run_list_of_tests(self, list_of_ids):
        for test_id in list_of_ids:
            self.run_test(test_id)

    def get_results(self, are_custom_tests=False):
        all_tests = len(self.test_order)
        performed_tests = 0
        run_successful = 0
        extended_results = []

        for test_id in self.test_order:
            test = self.test_collection[test_id]
            if test.executed:
                performed_tests += 1
                if test.is_ran_successful():
                    run_successful += 1
                    extended_results += ["%-70.70s: ACCEPTED" % self.test_collection[test_id].title]
                else:
                    extended_results += ["%-70.70s: FAILED" % self.test_collection[test_id].title]

        print("---RESULTS---")
        print("Total {} tests".format(all_tests))
        print("Executed {} tests".format(performed_tests))
        print("Passed {} tests".format(run_successful))
        print("-------------")
        for result in extended_results:
            print(result)
        print("-------------")

        if are_custom_tests:
            return performed_tests - run_successful
        return all_tests - run_successful

    def debug(self, debug):
        self.context["DEBUG"] = debug
