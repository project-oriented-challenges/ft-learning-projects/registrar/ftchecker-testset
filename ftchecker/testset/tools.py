from subprocess import check_output, CalledProcessError, TimeoutExpired, STDOUT
from ftchecker.testset.WrongAnswerException import WrongAnswerException


def runCmd(args, with_python=True, no_out=False, timeout=120):
    executed = False
    out = ""
    ext_args = args[:]
    if with_python:
        ext_args.insert(0, "python")
    try:
        print("EXECUTED: {}".format(' '.join(args[:])))
        tmpout = check_output(ext_args, stderr=STDOUT, timeout=timeout)
    except CalledProcessError as ret:
        tmpout = ret.stdout
    except TimeoutExpired:
        tmpout = ''
    if (no_out and len(tmpout) == 0) or len(tmpout) > 0:
        executed = True
        out = tmpout.decode().splitlines()

    if not executed:
        raise Exception("The command executed too long or there is nothing on output")

    return out


def compare_answers(return_message, return_expected):
    if type(return_message) == str:
        result = return_expected.strip() == return_message.strip()
    elif type(return_message) == list:
        result = True
        if len(return_message) != len(return_expected):
            result = False
        for i in range(len(return_expected)):
            if return_expected != return_message:
                result = False
    else:
        result = return_message == return_expected
    if not result:
        if type(return_message) == list:
            return_message = "\n".join(return_message)
            return_expected = "\n".join(return_expected)

        print('--- ACTUAL RESPONSE:\n{}\n--- EXPECTED RESPONSE:\n{}'.format(return_message,
                                                                            return_expected))

    if not result:
        raise WrongAnswerException()
