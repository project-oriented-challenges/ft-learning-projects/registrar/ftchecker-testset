from eth_account import Account
from time import sleep
from web3 import Web3
from uuid import uuid4 

MIN_BALANCE_PER_ACCOUNT = 0.1  # in ether


class AccountManagement:
    def __init__(self, _privkey):
        if len(_privkey) != 64:
            raise Exception("Incorrect private key")
        self.primary = Account.privateKeyToAccount(_privkey)
        self.accounts = [self.primary]

    def generate(self, _w3, _num, _random = False):
        if _w3.eth.getBalance(self.primary.address) < _w3.toWei(_num * MIN_BALANCE_PER_ACCOUNT, 'ether'):
            raise Exception("Not enough balance on the primary account")
        self.accounts = [self.primary]
        if _num > 1:
            if not _random:
                tmp_pk = _w3.keccak(self.primary.privateKey)
            else:
                tmp_pk = _w3.keccak(self.primary.privateKey + uuid4().bytes)
            for i in range(_num - 1):
                self.accounts.append(Account.privateKeyToAccount(tmp_pk))
                self.refill_balance(_w3, i + 1)
                tmp_pk = _w3.keccak(tmp_pk)

    def add_account(self, _nested=True):
        if _nested:
            acc = Account.privateKeyToAccount(Web3.keccak(self.accounts[-1].privateKey))
        else:
            acc = Account.create(Web3.keccak(self.accounts[-1].privateKey))
        self.accounts.append(acc)

        return acc.address

    def add_account_by_private_key(self, private_key):
        acc = Account.privateKeyToAccount(private_key)
        self.accounts.append(acc)
        return acc

    def get_accounts(self):
        return [i.address for i in self.accounts]

    def get_account(self, _id):
        if type(_id) == int:
            if _id < len(self.accounts):
                acc = self.accounts[_id]
            else:
                raise Exception("No account with such number")
        elif type(_id) == str:
            acc = None
            for i in self.accounts:
                if i.address.lower() == _id.lower():
                    acc = i
                    break
            if acc is None:
                raise Exception("No account with such address")
        return acc

    def _sendtx(self, _w3, _pk, _to, _value):
        sender = _w3.eth.account.privateKeyToAccount(_pk).address

        loopCounter = 5
        while loopCounter > 0:

            nonce = _w3.eth.getTransactionCount(sender)

            transaction = {
                'to': _to,
                'value': _value,
                'gasPrice': _w3.toWei(1, 'gwei'),
                'gas': 21000,
                'nonce': nonce,
            }

            signed = _w3.eth.account.signTransaction(transaction, _pk)
            try:
                resp = _w3.eth.sendRawTransaction(signed.rawTransaction)
            except ValueError as e:
                print(f'Something went wrong to send {_value} from {sender} to {_to} with nonce {nonce}: ' + str(e))
                sleep(1)
            except:
                raise
            else:
                break

            loopCounter = loopCounter - 1

        receipt = _w3.eth.waitForTransactionReceipt(resp)
        if receipt is None:
            raise Exception(f'No transaction with {resp.hex()} confirmed')
        if receipt.status == 0:
            raise Exception(f'Transaction {resp.hex()} confirmed but failed')

        while nonce == _w3.eth.getTransactionCount(sender):
            sleep(0.1)

    def adjust_balance(self, _w3, _account, _required_balance, _limit=False):
        receipient = self.get_account(_account)
        to_send = _required_balance - _w3.eth.getBalance(receipient.address)
        if to_send > 0:
            if _w3.eth.getBalance(self.primary.address) < (to_send + _w3.toWei(21000, 'gwei')):
                raise Exception(f'No enough funds on the primary account {self.primary.address}')
            else:
                self._sendtx(_w3, self.primary.privateKey, receipient.address, to_send)
        elif to_send < 0:
            to_send = to_send + _w3.toWei(21000, 'gwei')
            if _limit and (to_send < 0):
                self._sendtx(_w3, receipient.privateKey, self.primary.address, -to_send)

    def refill_balance(self, _w3, _account):
        to_send = _w3.toWei(MIN_BALANCE_PER_ACCOUNT, 'ether')
        self.adjust_balance(_w3, _account, to_send)

    def refill_address_balance(self, _w3, _address, to_send):
        if _w3.eth.getBalance(self.primary.address) < (to_send + _w3.toWei(21000, 'gwei')):
            raise Exception(f'No enough funds on the primary account {self.primary.address}')
        else:
            self._sendtx(_w3, self.primary.privateKey, _address, to_send)

    def refund(self, _w3):
        if len(self.accounts) == 1:
            return
        for i in self.accounts[1:]:
            self.refund_one(_w3, i)

    def refund_one(self, _w3, _account):
        if type(_account) in [int, str]:
            sender = self.get_account(_account)
        else:
            sender = _account
        to_send = _w3.eth.getBalance(sender.address) - _w3.toWei(21000, 'gwei')
        if to_send > 0:
            self._sendtx(_w3, sender.privateKey, self.primary.address, to_send)
