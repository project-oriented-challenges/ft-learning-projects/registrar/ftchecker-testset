from setuptools import setup


setup(
    name='ftchecker-testset',

    scripts=[
    ],


    version='1.0',

    description='Framework to automate of solution checking for ONTI FinTech assignments',
    url='https://gitlab.com/registrar/ftchecker-testset',

    author='Matvey Plevako',
    author_email='matveyplevako@mail.ru',

    license='MIT',

    packages=[
        'ftchecker.testset',
    ],
    include_package_data=True,

    install_requires=[
        'web3>=5.0.0a3'
    ],

    zip_safe=False,
)
